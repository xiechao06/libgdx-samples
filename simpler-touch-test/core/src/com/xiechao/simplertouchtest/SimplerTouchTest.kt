package com.xiechao.simplertouchtest

import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.ExtendViewport

class SimplerTouchTest : ApplicationAdapter(), InputProcessor {


    /** Called when the mouse was moved without any buttons being pressed. Will not be called on iOS.
     * @return whether the input was processed
     */
    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    /** Called when a key was typed
     *
     * @param character The character
     * @return whether the input was processed
     */
    override fun keyTyped(character: Char): Boolean {
        return false
    }

    /** Called when the mouse wheel was scrolled. Will not be called on iOS.
     * @param amount the scroll amount, -1 or 1 depending on the direction the wheel was scrolled.
     * @return whether the input was processed.
     */
    override fun scrolled(amount: Int): Boolean {
        return false
    }

    /** Called when a key was released
     *
     * @param keycode one of the constants in [Input.Keys]
     * @return whether the input was processed
     */
    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    /** Called when a key was pressed
     *
     * @param keycode one of the constants in [Input.Keys]
     * @return whether the input was processed
     */
    override fun keyDown(keycode: Int): Boolean {
        return false
    }

    /** Called when the screen was touched or a mouse button was pressed. The button parameter will be [Buttons.LEFT] on iOS.
     * @param screenX The x coordinate, origin is in the upper left corner
     * @param screenY The y coordinate, origin is in the upper left corner
     * @param pointer the pointer for the event.
     * @param button the button
     * @return whether the input was processed
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (button != Input.Buttons.LEFT || pointer > 0) {
            return false
        }
        Gdx.app.debug(TAG, "screenX: $screenX, screenY: $screenY")
        camera.unproject(tp.apply {
            x = screenX.toFloat()
            y = screenY.toFloat()
        })
        dragging = true
        return true
    }

    /** Called when a finger was lifted or a mouse button was released. The button parameter will be [Buttons.LEFT] on iOS.
     * @param pointer the pointer for the event.
     * @param button the button
     * @return whether the input was processed
     */
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (button != Input.Buttons.LEFT || pointer > 0) {
            return false
        }
        camera.unproject(tp.apply {
            x = screenX.toFloat()
            y = screenY.toFloat()
        })
        dragging = false
        return true
    }

    /** Called when a finger or the mouse was dragged.
     * @param pointer the pointer for the event.
     * @return whether the input was processed
     */
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        if (!dragging) return false
        camera.unproject(tp.apply { x = screenX.toFloat(); y = screenY.toFloat() })
        return true
    }

    companion object {
        const val SCALE = 32F
        const val INV_SCALE = 1 / SCALE
        const val VP_WIDTH = 1280 / SCALE
        const val VP_HEIGHT = 720 / SCALE
        const val TAG = "SimplerTouchTest"
    }

    lateinit var camera: OrthographicCamera
    lateinit var viewport: ExtendViewport
    lateinit var shapes: ShapeRenderer
    lateinit var tp: Vector3
    var dragging = false

    override fun create() {
        Gdx.app.logLevel = Application.LOG_DEBUG
        Gdx.app.log("SimplerTouchTest", "world!")
        camera = OrthographicCamera()
        viewport = ExtendViewport(VP_WIDTH, VP_WIDTH, camera)
        shapes =  ShapeRenderer()
        tp = Vector3(0F, 0F, 0F)
        Gdx.input.inputProcessor = this
    }

    override fun render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        shapes.projectionMatrix = camera.combined
        shapes.begin(ShapeRenderer.ShapeType.Filled)
        shapes.circle(tp.x, tp.y, 0.25F, 16)
        shapes.end()
    }

    override fun dispose() {
        shapes.dispose()
    }

    override fun resize(width: Int, height: Int) {
        // viewport must be updated for it to work properly
        viewport.update(width, height, true)
    }

}
