package com.xiechao.running_2d_animation

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.Array

class Running2DAnimation : ApplicationAdapter() {
    lateinit var batch: SpriteBatch
    lateinit var texture: Texture
    lateinit var animation: Animation<TextureRegion>

    var currentTime = 0F

    companion object {
        const val COLS = 6
        const val ROWS = 5
    }

    override fun create() {
        val texture = Texture(Gdx.files.internal("sprite-animation4.png"))
        val frames = Array<TextureRegion>(
            TextureRegion.split(texture,
                    texture.width / COLS,
                    texture.height/ ROWS).flatten()
                    .toTypedArray()
        )
        animation = Animation(0.05f, frames)
        batch = SpriteBatch()
    }

    override fun render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        currentTime += Gdx.graphics.deltaTime
        val frame = animation.getKeyFrame(currentTime, true)
        batch.begin()
        batch.draw(frame, 150f, 150f)
        batch.end()
    }

    override fun dispose() {
        batch.dispose()
        texture.dispose()
    }
}
