package com.badlogic.drop

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.TimeUtils
import kotlin.random.Random

class Drop : ApplicationAdapter() {
    private lateinit var dropImage: Texture
    private lateinit var bucketImage: Texture
    private lateinit var dropSound: Sound
    private lateinit var rainMusic: Music
    private lateinit var camera: OrthographicCamera
    private lateinit var batch: SpriteBatch
    private lateinit var bucket: Rectangle
    private var lastDropTime: Long = 0
    private val raindrops: Array<Rectangle> = Array()
    private val random = Random(TimeUtils.nanoTime())

    override fun create() {
        dropImage = Texture(Gdx.files.internal("drop.png"))
        bucketImage = Texture(Gdx.files.internal("bucket.png"))
        dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"))
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"))

        camera = OrthographicCamera().apply {
            setToOrtho(false, 800F, 480F)
        }

        batch = SpriteBatch()
        bucket = Rectangle().apply {
            x = ((800 - 64) / 2).toFloat()
            y = 20F
            width = 64F
            height = 64F
        }

        raindrops.add(spawnRaindrop())
        lastDropTime = TimeUtils.nanoTime()

        rainMusic.isLooping = true
        rainMusic.play()
    }

    private fun spawnRaindrop() = Rectangle().apply {
        x = random.nextDouble((800 - 64).toDouble()).toFloat()
        y = 480F
        width = 64F
        height = 64F
    }

    override fun render() {
        Gdx.gl.glClearColor(0F, 0F, 0.2F, 1F)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        batch.projectionMatrix = camera.combined

        batch.begin()
        raindrops.forEach {
            batch.draw(dropImage, it.x, it.y)
        }
        batch.draw(bucketImage, bucket.x, bucket.y)
        batch.end()

        if (Gdx.input.isTouched) {
            val touchPos = Vector3().apply {
                x = Gdx.input.x.toFloat()
                y = Gdx.input.y.toFloat()
                camera.unproject(this)
            }

            bucket.x = touchPos.x - 64 / 2
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            bucket.x -= 200 * Gdx.graphics.deltaTime
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            bucket.x += 200 * Gdx.graphics.deltaTime
        }
        if (bucket.x < 0 ) {
            bucket.x = 0F
        }
        if (bucket.x > 800 - 64){
            bucket.x = (800 - 64).toFloat()
        }
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) {
            raindrops.add(spawnRaindrop())
            lastDropTime = TimeUtils.nanoTime()
        }
        val iter = raindrops.iterator()
        while (iter.hasNext()) {
            val raindrop = iter.next()
            raindrop.y -= 200 * Gdx.graphics.deltaTime
            if (raindrop.y + 64 < 0) {
               iter.remove()
            }
            if (raindrop.overlaps(bucket)) {
                dropSound.play()
                iter.remove()
            }
        }
    }

    override fun dispose() {
        dropImage.dispose()
        bucketImage.dispose()
        dropSound.dispose()
        rainMusic.dispose()
        batch.dispose()
    }
}
